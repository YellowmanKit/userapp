import MMKVStorage from 'react-native-mmkv-storage'
import { to } from 'userapp/src/utility/Func'
const MMKV = new MMKVStorage.Loader().initialize()

var err, data
export const read = async () => {
  [err, data] = await to(MMKV.getStringAsync('Records'))
  if(err){ console.log(err); return err }
  return data? data.split(','):[]
}

export const write = async records => {
  [err, data] = await to(MMKV.setStringAsync('Records', records))
  if(err){ console.log(err) }
  return data
}

export const clear = async () => {
  [err, data] = await to(MMKV.clearStore())
  if(err){ console.log(err) }
  return data
}
