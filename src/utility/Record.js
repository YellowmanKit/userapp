import { read, write } from 'userapp/src/utility/File'

export const addRecord = async (app, data) => {
  const { state: { view, records } } = app
  var newRecords = await read()
  if(data){ newRecords.push(data) }
  records.set(newRecords)
  if(newRecords){ write(newRecords.join()) }
}
