import React, {
  useState
} from 'react'

export const size = (width, height) => { return { width, height } }
export const to = promise => promise.then(data => [null, data] ).catch(err => [err])
export const addState = (app, name, def) => {
  const [value, set] = useState(def)
  app['state'][name] = { value, set }
}
