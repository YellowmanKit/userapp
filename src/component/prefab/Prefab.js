import React from 'react'
import {
  View as Div,
  Text,
  TouchableOpacity,
  Image
} from 'react-native'
const minibusIcon = require('userapp/res/minibus.jpg')
import { size } from 'userapp/src/utility/Func'
import { ui } from 'userapp/src/redux/ui/store'

export const info = text => {
  return(
    <Div style={{ width: 300, backgroundColor: 'yellow' }}>
      <Text style={{ fontSize: 20, color: 'grey', textAlign: 'center', padding: 10 }}>{text}</Text>
    </Div>
  )
}

export const hint = text => {
  return(
    <Div style={{ width: 300 }}>
      <Text style={{ fontSize: 20, color: 'grey', textAlign: 'center' }}>{text}</Text>
    </Div>
  )
}

export const status = text => {
  return(
    <Div style={{ width: 300 }} key={text}>
      <Text style={{ fontSize: 30, color: 'red', textAlign: 'center' }}>{text}</Text>
    </Div>
  )
}

export const record = text => {
  return(
    <Div style={{ ...ui.style.flexColSC, width: 350 }} key={text}>
      <Div style={{ ...ui.style.flexRowCC, flex: 1 }}>
        <Image source={minibusIcon} style={{  width: 25, height: 25 }}/>
        <Div style={{ width: 10 }}/>
        <Div style={{ width: 300 }}>
          <Text style={{ fontSize: 15, color: 'grey' }}>{text}</Text>
        </Div>
      </Div>
      {sep.ver('100%')}
    </Div>
  )
}

export const btn = (text, onPress) => {
  return (
    <TouchableOpacity onPress={onPress}
    style={{ ...ui.style.flexColCC, height: 50, padding: 40, backgroundColor: 'green'}}>
      <Text style={{ color: 'white', fontSize: 20, textAlign: 'center' }}>{text}</Text>
    </TouchableOpacity>
  )
}

export const gap = {
  ver: height => <Div style={{ height }}/>
}

export const sep = {
  ver: width => <Div style={{ width, height: 1, backgroundColor: 'grey', flexShrink: 0 }}/>
}
