import React from 'react'
import {
  View as Div,
  NativeModules
} from 'react-native'
const { DecisionModule } = NativeModules

import { gap, btn, info } from 'userapp/src/component/prefab/Prefab'
import { to, size, addState } from 'userapp/src/utility/Func'

const Start = app => {
  const { store: { ui: { style } }, state: { view, device } } = app
  const { manu, model, system, osVersion } = device.value
  return (
    <Div style={{ ...style.flexColCC, ...size('100%', '100%') }}>
      {info('Manufacturer: ' + manu + '\nModel: ' + model + '\nSystem: ' + system + '\nVersion: ' + osVersion)}
      {gap.ver('5%')}
      {btn('開始\nStart', ()=>{
        view.set('home')
        DecisionModule.turnOn()
      })}
    </Div>
  )
}

export default Start
