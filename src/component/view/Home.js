import React from 'react'
import {
  View as Div,
  BackHandler
} from 'react-native'

import { gap, btn, hint } from 'userapp/src/component/prefab/Prefab'
import { size } from 'userapp/src/utility/Func'
import { read } from 'userapp/src/utility/File'

const Home = app => {
  const { store: { ui: { style } }, state: { view } } = app
  return (
    <Div style={{ ...style.flexColCC, ...size('100%', '100%')  }}>
      {hint('Tap title to simulate event')}
      {gap.ver('5%')}
      {btn('看紀錄\nSee Record', ()=>{ view.set('record') })}
      {gap.ver('5%')}
      {btn('終止系統\nEnd & Exit Systen', ()=>{ BackHandler.exitApp() })}
    </Div>
  )

}

export default Home
