import React from 'react'
import {
  View as Div
} from 'react-native'

import { gap, btn, status } from 'userapp/src/component/prefab/Prefab'
import { size } from 'userapp/src/utility/Func'
import { addRecord } from 'userapp/src/utility/Record'

const Aboard = app => {
  const { store: { ui: { style } }, state: { view } } = app

  return (
    <Div style={{ ...style.flexColCC, ...size('100%', '100%')  }}>
      {btn('登上小巴\nIn Minibus', ()=>{
        addRecord(app, 'Type: MiniBus Number: XY123 Check in time: ' + Date.now())
        view.set('unload')
      })}
      {gap.ver('5%')}
      {status('ABC123\n11:21:15 AM\n20/2/2021')}
    </Div>
  )

}

export default Aboard
