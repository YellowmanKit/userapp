import React from 'react'
import {
  View as Div
} from 'react-native'

import { gap, btn, status } from 'userapp/src/component/prefab/Prefab'
import { size } from 'userapp/src/utility/Func'
import { addRecord } from 'userapp/src/utility/Record'

const Unload = app => {
  const { store: { ui: { style } }, state: { view } } = app

  return (
    <Div style={{ ...style.flexColCC, ...size('100%', '100%')  }}>
      {btn('離開小巴\nLeave Minibus', ()=>{
        addRecord(app, 'Type: MiniBus Number: XY123 Check-out Time: ' + Date.now())
        view.set('home') 
      })}
      {gap.ver('5%')}
      {status('ABC123\n11:32:45 AM\n20/2/2021')}
    </Div>
  )

}

export default Unload
