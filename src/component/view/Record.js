import React, { Component } from "react"
import {
  View as Div,
  ScrollView as SV
} from 'react-native'

import { gap, sep, btn, record } from 'userapp/src/component/prefab/Prefab'
import { size, addState, to } from 'userapp/src/utility/Func'
import { read, write, clear } from 'userapp/src/utility/File'

const Record = app => {
  const { store: { ui: { style } }, state: { view, records } } = app
  return (
    <Div style={{ ...style.flexColCC, ...size('100%', '100%')  }}>
      {gap.ver('3%')}
      <SV>
        {records.value.map(data => record(data))}
      </SV>
      {gap.ver('3%')}
      {btn('清除紀錄\nClear Record', ()=>{ clear(); records.set([]) })}
      {gap.ver('3%')}
      {btn('返回主目錄\nReturn to Menu', ()=>{ view.set('home') })}
      {gap.ver('3%')}
    </Div>
  )
}
export default Record
