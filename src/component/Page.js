import React, {
  useEffect
} from 'react'
import {
  View as Div,
  SafeAreaView as SAV,
  Text,
  DeviceEventEmitter,
  NativeModules
} from 'react-native'
import DeviceInfo from 'react-native-device-info';

const { DecisionModule } = NativeModules

import { gap, btn } from 'userapp/src/component/prefab/Prefab'
import { to, size, addState } from 'userapp/src/utility/Func'
import { read } from 'userapp/src/utility/File'
import { addRecord } from 'userapp/src/utility/Record'

import Start from './view/Start'
import Home from './view/Home'
import Record from './view/Record'
import Aboard from './view/Aboard'
import Unload from './view/Unload'

const Page = app => {
  const title = '更安心出行\n小巴自動紀錄系統\nTravelSafer Minibus\nAutomactic Recording System'
  addState(app, 'view', 'start')
  addState(app, 'records', [])
  addState(app, 'device', {})

  const { store: { ui: { style } }, state: { view, records, device } } = app

  useEffect(()=>{
    recordListener(app)
    getDeviceInfo(app)
    DecisionModule.createDecisionModule()
  }, [])

  return (
    <SAV style={{ ...style.flexColCS, ...size('100%', '100%')  }}>
      {gap.ver('10%')}
      <Text style={{ color: 'green', textAlign: 'center', fontSize: 25 }}>{title}</Text>
      <Div style={{ ...style.flexColCC, flex: 1 }}>
        {views(app)}
      </Div>
    </SAV>
  )

}

const views = (app) => {
  const { state: { view } } = app
  switch (view.value) {
    case 'start':
      return Start(app)
    case 'home':
      return Home(app)
    case 'record':
      return Record(app)
    case 'aboard':
      return Aboard(app)
    case 'unload':
      return Unload(app)
    default:
      return null
  }
}

const recordListener = app =>{
  var mounted = true
  const { state: { view, records }} = app
  DeviceEventEmitter.addListener('EventDidRangeBeaconsInRegion', data => {
    if(mounted){
      console.log(data)
      //view.set('aboard')
      //addRecord(app, data)
    }
  })
  const getRecords = async ()=>{
    const data = await read()
    if(mounted){ records.set(data) }
  }
  getRecords()
  return function cleanup() { mounted = false }
}

const getDeviceInfo = app => {
  var mounted = true
  const { state: { device }} = app
  const model = DeviceInfo.getModel()
  const osVersion = DeviceInfo.getSystemVersion()
  const system = DeviceInfo.getSystemName()
  const getManu = async ()=> {
    var err, data;
    [err, data] = await to(DeviceInfo.getManufacturer())
    if(mounted){ device.set({ model, system, osVersion, manu: data }) }
  }
  getManu()
  return function cleanup() { mounted = false }
}

export default Page
