export const ui = {
  style: {
    flexColCC: { flexDirection: 'column', alignItems: 'center', justifyContent: 'center' },
    flexColCS: { flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' },
    flexColSC: { flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' },

    flexRowCC: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
    flexRowCS: { flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' },
  }
}
