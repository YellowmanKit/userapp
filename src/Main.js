import React from 'react'
import Page from './component/Page'
import store from 'userapp/src/redux/store'

const Main = () => Page({
  store,
  state: {}
})

export default Main
