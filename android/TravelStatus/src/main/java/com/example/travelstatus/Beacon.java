package com.example.travelstatus;
import com.facebook.react.bridge.ReactApplicationContext;

import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanCallback;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

public class Beacon extends TSModule {

    @NonNull
    @Override
    public String getName() {
        return "Main";
    }

    public Beacon(ReactApplicationContext context){
        super(context);
    }

    public String getStatus(){
        if (!checkBluetooth()) {
            return "No bluetooth";
        }
        scanLeDevice();
        return "Scanning...";
    }

    boolean checkBluetooth(){
        if (!reactContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            return false;
        }
        final BluetoothManager bluetoothManager = (BluetoothManager) reactContext.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            //Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            //reactContext.startActivityForResult(enableBtIntent, 0, Bundle.EMPTY);
            return false;
        }
        return true;
    }

    private final BluetoothLeScanner bluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
    private boolean mScanning;
    private static final long SCAN_PERIOD = 10000;

    private void scanLeDevice() {
        log("scanLeDevice");
        if (!mScanning) {
            log("startScanning");
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    log("stopScanning");
                    mScanning = false;
                    bluetoothLeScanner.stopScan(leScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            bluetoothLeScanner.startScan(leScanCallback);
        } else {
            log("stopScanning");
            mScanning = false;
            bluetoothLeScanner.stopScan(leScanCallback);
        }
    }

    private final ScanCallback leScanCallback =
            new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    log("ScanCallback");
                    log(String.valueOf(callbackType));
                }
            };

}
