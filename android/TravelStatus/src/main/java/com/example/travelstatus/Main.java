package com.example.travelstatus;

import androidx.annotation.NonNull;
import com.facebook.react.bridge.ReactApplicationContext;

public class Main extends TSModule {

    @NonNull
    @Override
    public String getName() {
        return "Permission";
    }

    public Main(ReactApplicationContext context){
        super(context);
    }

    public String getStatus(){
        final Permission permission = new Permission(reactContext);
        return permission.check();
        /*Beacon beacon = new Beacon(this.mContext);
        return beacon.getStatus();*/
    }

}
