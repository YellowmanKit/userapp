package com.example.travelstatus;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import com.facebook.react.bridge.ReactApplicationContext;

public class Permission extends TSModule {

    @NonNull
    @Override
    public String getName() {
        return "Permission";
    }

    public Permission(ReactApplicationContext context){
        super(context);
    }

    public String check(){
        if (ContextCompat.checkSelfPermission(reactContext,
                Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            return "BLUETOOTH permission not granted";
        }
        if (ContextCompat.checkSelfPermission(reactContext,
                Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
            return "BLUETOOTH_ADMIN permission not granted";
        }
        if (ContextCompat.checkSelfPermission(reactContext,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return "ACCESS_FINE_LOCATION permission not granted";
        }
        return "All permission granted";
    }

}
