package com.example.travelstatus;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import android.widget.Toast;

public abstract class TSModule extends ReactContextBaseJavaModule {

    protected ReactApplicationContext reactContext;

    TSModule(ReactApplicationContext context){
        super(context);
        this.reactContext = context;
    }

    protected void log(String text){
        Toast.makeText(reactContext, text, Toast.LENGTH_SHORT).show();
    }

}
