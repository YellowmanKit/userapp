package com.userapp;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import android.Manifest;
import android.content.BroadcastReceiver;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.location.DetectedActivity;
import com.travelsafer.decisionstatus.makingDecision;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;


public class DecisionModule extends ReactContextBaseJavaModule implements BeaconConsumer, SensorEventListener {
    private static ReactApplicationContext reactContext;
    private static final int PERMISSION_REQUEST_FINE_LOCATION =1;
    private static final int PERMISSION_ACTIVITY_RECOGNITION = 2;
    private makingDecision MD = new makingDecision();
    private BeaconManager beaconManager;
    private SensorManager mSensorManager;
    private BroadcastReceiver broadcastReceiver;
    private String userActivity = "null";
    private float[] mA = new float[3];
    private float[] mG = new float[3];
    public boolean debugButton = false;
    public boolean offButton = false;
    public boolean onButton=true;
    private String record = "";
    public String recordDebug = "";
    public  String finalRecord ="";
    private String hasBeacon = "false";
    String[] permissions = new String[]{};
    List<String> mPermissionList = new ArrayList<>();

    @ReactMethod
    DecisionModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
                   // , Manifest.permission.ACTIVITY_RECOGNITION};
            //initPermission();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION};
                    //, Manifest.permission.ACTIVITY_RECOGNITION};
            //initPermission();
        }

    }
    @Override
    public String getName() {
        return "DecisionModule";
    }

    @ReactMethod
    public void turnOn(){
        offButton=false;
        debugButton=false;
        onButton=true;
        LocalBroadcastManager.getInstance(reactContext).registerReceiver(broadcastReceiver, new IntentFilter("activity_intent"));
        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mSensorManager.registerListener(this, accelerometer,
                        SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
            }
        }
        Sensor gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        if (gravity != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mSensorManager.registerListener(this, gravity,
                        SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI);
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @ReactMethod
    public void turnOff() {
        offButton=true;
        debugButton=false;
        onButton=false;
        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            mSensorManager.unregisterListener(this, accelerometer);
        }
        Sensor gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        if (gravity != null) {
            mSensorManager.unregisterListener(this, gravity);
        }

        beaconManager.unbind(this);
        LocalBroadcastManager.getInstance(reactContext).unregisterReceiver(broadcastReceiver);
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    private void initPermission() {
        mPermissionList.clear();
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(reactContext, permissions[i]) !=
                    PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }
        if (mPermissionList.size() > 0) {
            ActivityCompat.requestPermissions(reactContext.getCurrentActivity(), permissions, 100);
        }

    }

    public void scanBeacons() {
        beaconManager = BeaconManager.getInstanceForApplication(reactContext);
        beaconManager.getBeaconParsers().clear();
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:0-3=4c000215,i:4-19,i:20-21,i:22-23,p:24-24"));
        //beaconManager.setDebug(true);

        /*Notification.Builder builder = new Notification.Builder(reactContext);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("Scanning for Beacons");
        Intent intent = new Intent(reactContext, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                reactContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("My Notification Channel ID",
                    "My Notification Name", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("My Notification Channel Description");
            NotificationManager notificationManager = (NotificationManager) reactContext.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        beaconManager.enableForegroundServiceScanning(builder.build(), 456);*/

        //beaconManager.setEnableScheduledScanJobs(false);
        //beaconManager.setBackgroundBetweenScanPeriod(0);
        beaconManager.setBackgroundBetweenScanPeriod(1000);
        beaconManager.setForegroundScanPeriod(1000);
        beaconManager.bind(this);

        mSensorManager = (SensorManager)reactContext.getSystemService(Context.SENSOR_SERVICE);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("activity_intent")) {
                    int type = intent.getIntExtra("type", -1);
                    int confidence = intent.getIntExtra("confidence", 0);
                    handleUserActivity(type, confidence);
                }
            }

        };
        startTracking();
        turnOn();
    }

    private void startTracking() {
        Intent intent = new Intent(reactContext, BackgroundDetectedActivitiesService.class);
        reactContext.startService(intent);
    }

    private void handleUserActivity(int type, int confidence) {
        String label = "";
        switch (type) {
            case DetectedActivity.IN_VEHICLE: {
                label = "activity_in_vehicle";
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                label = "activity_on_bicycle";
                break;
            }
            case DetectedActivity.ON_FOOT: {
                label = "activity_on_foot";
                break;
            }
            case DetectedActivity.RUNNING: {
                label = "activity_running";
                break;
            }
            case DetectedActivity.STILL: {
                label = "activity_still";
                break;
            }
            case DetectedActivity.TILTING: {
                label = "activity_tilting";
                break;
            }
            case DetectedActivity.WALKING: {
                label = "activity_walking";
                break;
            }
            case DetectedActivity.UNKNOWN: {
                label = "activity_unknown";
                break;
            }
        }
        //Log.e(TAG, "User activity: " + label + ", Confidence: " + confidence);
        if (confidence > 70) {
            userActivity = label;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @ReactMethod
    public void createDecisionModule() {
        debugButton= false;
        offButton = false;
        onButton = true;
        finalRecord = null;
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (reactContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(Objects.requireNonNull(getCurrentActivity()),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_REQUEST_FINE_LOCATION);
            }
            if (reactContext.checkSelfPermission(Manifest.permission.ACTIVITY_RECOGNITION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(Objects.requireNonNull(getCurrentActivity()),new String[]{Manifest.permission.ACTIVITY_RECOGNITION},PERMISSION_ACTIVITY_RECOGNITION);
            }
        }*/
        scanBeacons();

    }
    @ReactMethod
    public void debugEvent (){
        debugButton=true;
        offButton =false;
        onButton= false;
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(final Collection<Beacon> beacons, Region region) {
                //record = MD.decisionStatus(beacons, userActivity,mA,mG);
                if(offButton == false) {
                    if (debugButton==true) {
                        finalRecord = MD.decisionStatusDebug(beacons, userActivity, mA, mG);

                    } else {
                        String recordStatus;
                        recordStatus = MD.decisionStatus(beacons, userActivity, mA, mG);
                        if (recordStatus.length() != 0) {
                            finalRecord = recordStatus.toString();
                        }
                    }



                    WritableMap newParams = Arguments.createMap();
                    newParams.putString("Status", finalRecord);
                    sendEvent(reactContext, "EventDidRangeBeaconsInRegion", newParams);
                }
                if (beacons.size()>0){
                    hasBeacon = "";
                }
                else {
                    hasBeacon = "no beacon";
                }
            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, Identifier.parse("7275"), null));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {

    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            System.arraycopy(event.values,0,mA,0,3);
        }
        if (event.sensor.getType() == Sensor.TYPE_GRAVITY){
            System.arraycopy(event.values,0,mG,0,3);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
