package com.userapp;

import androidx.annotation.NonNull;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RNModule extends ReactContextBaseJavaModule {

    @NonNull
    @Override
    public String getName() {
        return "RNModule";
    }

    ReactApplicationContext reactContext;
    RNModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

}